import './App.css';

import { Header } from './Components/Header/Header';
import { Main } from './Main';
import { Footer } from './Components/Footer/Footer'

function App() {
  return (
    <div className="App">
      <div className="main"><Header/></div>
      <div><Main/></div>
      <div><Footer/></div>
    </div>
  );
}

export default App;
