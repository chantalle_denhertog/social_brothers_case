import React from 'react';
import { Routes, Route } from 'react-router-dom';

import { Blog } from './Components/Blog/Blog';
import { Home } from './Components/Home/Home';

export const Main = () => {
  return (
    <div className="container">
      <Routes>
        <Route exact path='/' exact={true} activeClassName='is-active' element={<Home />}></Route>
        <Route path='/blog' activeClassName='is-active' element={<Blog />}></Route>
      </Routes>
    </div>
  );
}

export default Main;