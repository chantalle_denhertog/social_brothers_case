import { Component } from 'react';
import './Footer.scss';

export class Footer extends Component {

  render() {
      return (
        <footer>
          <p> &copy; Copyright Social Brothers - 2020 </p>
        </footer>
      )
  }
}