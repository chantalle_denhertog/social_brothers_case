import { Component } from 'react';
import { NavLink } from "react-router-dom";
import './Header.scss';

export class Header extends Component {

  render() {
      return (
          <header className="showcase">
              <nav >
                  <div className="logo">Logo</div>
                  <ul>
                      <li><NavLink  to="/">Home</NavLink></li>
                      <li><NavLink  to="/blog">Blog</NavLink></li>
                  </ul>
              </nav>
          </header>
      )
  }
}